Para instalar o material theme, copie e cole na pasta de plugin do software que você vai utilizar.
exp: C:\Users\youruser\.PhpStorm2018.2\config\plugins

!!!!ATENÇÃO, essa pasta só é criada após iniciar o software pelo menos uma vez.

Após colar a pasta reinicie o software, você será apresentado para uma tela, para que possa configurar o thema a seu gosto, 
caso queira usar uma das cores que estão disponiveis vá em "file -> import settings -> e aponte onde o arquivo está localizado.
após isso ele vai reiniciar e já vai estar com a cor aplicada.